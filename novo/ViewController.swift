//
//  ViewController.swift
//  novo
//
//  Created by COTEMIG on 27/09/43 AH.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"My key" , for: <#T##IndexPath#>)
        
        if indexPath.row % 2 == 0 {
        cell.backgroundColor = .red
        }
        return cell
    }
    
    @IBOutlet weak var tabelinha: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabelinha.dataSource=self
    }


}

